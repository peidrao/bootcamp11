
interface CreateUserData {
  name?: string; //opcional
  email: string;
  password: string;
  techs: Array<string>

}
export default function UserCreate({name, email, password, techs}: CreateUserData) {
  const user = {
    name, email, password, techs
  }
  return user;
}