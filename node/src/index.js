const express = require('express')
const {uuid} = require('uuidv4')

const app = express()
app.use(express.json());

const projects = [];

app.get('/projects', (req, res) => {
  return res.json(projects);
})

app.post('/projects', (req, res) => {
  const {name, age} = req.body;

  const nameExist = projects.findIndex(project => project.name === name);

  if(!nameExist) return res.status(400).json({erro: "user exists"});
  const project = { id: uuid(), name, age}
  projects.push(project);

  return res.json(project);
})

app.put('/projects/:id', (req, res) => {
  const {id} = req.params;
  const {name, age} = req.body;
  const projectIndex = projects.findIndex(project => project.id === id);

  if (projectIndex < 0) return res.status(404).json({ erro: "user not found" });

  const project = {id, name, age} 

  projects[projectIndex] = project;

  return res.json(project);

})

app.delete('/projects/:id', (req, res) => {
  const {id} =  req.params;

  const projectIndex = projects.findIndex(project => project.id === id);
  if (projectIndex < 0) return res.status(404).json({ erro: "user not found" });

  projects.splice(projectIndex, 1);

  return res.send();
})

const port = 8080;
app.listen(port, () => { console.log(`Servidor rodando na porta! ${port}`)})
