import User from '../infra/typeorm/entitites/User';
import ICreateIserDTO from '../dtos/ICreateUserDTO';

export default interface IUserRepository {
  findById(id: string): Promise<User | undefined>;
  findByEmail(email: string): Promise<User | undefined>;
  create(data: ICreateIserDTO): Promise<User>;
  save(user: User): Promise<User>;
}
