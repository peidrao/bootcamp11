import Appointment from '@modules/appointments/infra/typeorm/entities/Appointment';

import ICreateAppoitmentDTO from '../dtos/ICreateAppoitmentDTO';

export default interface IAppointmentsRepository {
  create(date: ICreateAppoitmentDTO): Promise<Appointment>;
  findByDate(date: Date): Promise<Appointment | undefined>;
}
