import AppError from '@shared/errors/appError';
import FakeAppointmentService from '../repositories/fakes/FakeAppointmentsRepository';
import CreateAppointmentService from './CreateAppoitmentService';

describe('CreateAppointment', () => {
  it('Should be abe to create a new appointment', async () => {
    const fakeAppointmentsRepository = new FakeAppointmentService();
    const createAppointment = new CreateAppointmentService(
      fakeAppointmentsRepository,
    );

    const appointment = await createAppointment.execute({
      date: new Date(),
      provider_id: '121312',
    });

    expect(appointment).toHaveProperty('id');
    expect(appointment.provider_id).toBe('121312');
  });

  it('should not be able to create two appointments on the same time', async () => {
    const fakeAppointmentsRepository = new FakeAppointmentService();
    const createAppointment = new CreateAppointmentService(
      fakeAppointmentsRepository,
    );

    const appointmentDate = new Date(2020, 4, 10, 11);

    await createAppointment.execute({
      date: appointmentDate,
      provider_id: '121312',
    });

    expect(
      createAppointment.execute({
        date: appointmentDate,
        provider_id: '121312',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
