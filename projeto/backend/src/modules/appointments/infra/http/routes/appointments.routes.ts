import { Router } from 'express';

import ensureAuthenticated from '@modules/users/infra/http/middlewares/ensureAuthenticated';
import AppointmentsControllers from '../controllers/AppointmentController';

const appointsmentsRouter = Router();
const appointmentController = new AppointmentsControllers();

appointsmentsRouter.use(ensureAuthenticated);

appointsmentsRouter.post('/', appointmentController.create);

export default appointsmentsRouter;
